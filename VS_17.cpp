﻿
#include <iostream>
#include <math.h>

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout <<"\nVector coordinates " << x << ' ' << y << ' ' << z << '\n';
    }
    double VectorModule()
    {
        double n = sqrt(x * x + y * y + z * z);        
        return n;
    }

private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector v(5, 8, 1); 
    v.Show();    
    std::cout << "Vector module " << v.VectorModule();
}